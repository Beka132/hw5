package com.example.davaleba5

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.davaleba5.databinding.ActivitySecondBinding

class SecondActivity:AppCompatActivity (){
    private lateinit var binding:ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivitySecondBinding.inflate((layoutInflater))
        setContentView(binding.root)
        binding.include.ivExit.setOnClickListener{
            finish()
        }
        binding.btnBack.setOnClickListener {
            finish()
        }
        init()
    }
    private fun init(){
        var profile=intent.getSerializableExtra("Extra_Profile") as Profile
        binding.tvName.setText(profile.firstName)
        binding.tvLastName.setText(profile.lastName)
        binding.tvAge.setText(profile.age.toString())
        binding.tvEmail.setText(profile.email)
    }
}
