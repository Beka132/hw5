package com.example.davaleba5

import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.example.davaleba5.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var index=0
    var profiles:MutableMap<Int,Profile> =mutableMapOf()
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }
    @RequiresApi(Build.VERSION_CODES.N)
    private fun init(){
        binding.btnAdd.setOnClickListener {
            if (binding.etName.text.isEmpty() || binding.etLastName.text.isEmpty() || binding.etAge.text.isEmpty() || binding.etEmail.text.isEmpty()) {
                binding.etName.setTextColor(Color.RED)
                binding.etLastName.setTextColor(Color.RED)
                binding.etAge.setTextColor(Color.RED)
                binding.etEmail.setTextColor(Color.RED)
                Toast.makeText(this, "Please fill all the fields that are given", Toast.LENGTH_SHORT).show()
            }else if(!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString()).matches()) {
                Toast.makeText(this, "email is not valid", Toast.LENGTH_SHORT).show()
                binding.etEmail.setTextColor(Color.RED)
            }else if(profiles.containsValue(Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString()))){
                Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show()
                binding.etName.setTextColor(Color.RED)
                binding.etLastName.setTextColor(Color.RED)
                binding.etAge.setTextColor(Color.RED)
                binding.etEmail.setTextColor(Color.RED)
            }else{
                profiles.put(index,Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString()))
                binding.etName.setTextColor(Color.GREEN)
                binding.etLastName.setTextColor(Color.GREEN)
                binding.etAge.setTextColor(Color.GREEN)
                binding.etEmail.setTextColor(Color.GREEN)
                Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
                index++
            }
        }
        binding.btnRemove.setOnClickListener {
            if (profiles.containsValue(Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString()))){
                var numberOfIndex=0
                for (i in 0..index){
                    if (profiles.get(i)==Profile(binding.etName.text.toString(),
                            binding.etLastName.text.toString(),
                            binding.etAge.text.toString().toInt(),
                            binding.etEmail.text.toString())){
                        numberOfIndex=i
                        break
                    }
                }
                profiles.remove(numberOfIndex,Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString()))
                binding.etName.setTextColor(Color.GREEN)
                binding.etLastName.setTextColor(Color.GREEN)
                binding.etAge.setTextColor(Color.GREEN)
                binding.etEmail.setTextColor(Color.GREEN)
                Toast.makeText(this, "User removed successfully", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show()
                binding.etName.setTextColor(Color.RED)
                binding.etLastName.setTextColor(Color.RED)
                binding.etAge.setTextColor(Color.RED)
                binding.etEmail.setTextColor(Color.RED)
            }
        }
        binding.btnUpdate.setOnClickListener {
            if (profiles.values.toString().contains(binding.etEmail.text.toString())){
                var numberOfIndex=0
                for (i in 0..index) {
                    if (profiles[i]?.email.toString() == binding.etEmail.text.toString()) {
                        numberOfIndex = i
                        break
                    }
                }
                profiles[numberOfIndex]=Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString())
                Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
            }else if(profiles.containsValue(Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString()))){
                Toast.makeText(this, "User with the exact same parameters already exists", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show()
            }
        }
        binding.button.setOnClickListener {
            Intent(this,SecondActivity::class.java).also {
                it.putExtra("Extra_Profile",Profile(binding.etName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etAge.text.toString().toInt(),
                    binding.etEmail.text.toString()))
                startActivity(it)
            }
        }
        binding.include.ivExit.setOnClickListener{
            onDestroy()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}