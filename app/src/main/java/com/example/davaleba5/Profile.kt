package com.example.davaleba5

import java.io.Serializable

data class Profile (var firstName:String,var lastName:String,var age:Int,val email:String):Serializable{
}